// console.log("Hello");

// Mock Database
let posts = [];

// Post ID
let count = 1;


//Add Post
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	//to prevent the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	count++;
	console.log(posts);
	alert("Successfully Added!");
	// invoke a function to show post later
	showPosts();
});


//Edit Post
const showPosts = () => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onClick="editPost('${post.id}')">Edit</button>
			<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// Edit Post Button
const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}


/************ ACTIVITY CODE START *************/ 
// Delete Post Button
const deletePost = (id) => {
	
	for(let i=0; i<posts.length; i++) {
		posts.splice([i].id, 1);
		showPosts();
		alert("Successfully Deleted!");
	}

};

/************ ACTIVITY CODE END *************/ 


// Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	//to prevent the page from loading
	e.preventDefault();

	for(let i=0; i<posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts();
			alert("Successfully Updated!");
			break;
		}
	}

});